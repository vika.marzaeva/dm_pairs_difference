# coding=utf8
import os
import json
import requests


def load_points(file_path):
    points = []
    with open(file_path) as file:
        for line in file.readlines()[1:]:
            words = line.strip().split(",")
            points.append({"x1": float(words[0]), "y1": float(words[1]), "x2": float(words[2]), "y2": float(words[3])})
    return points


def load_dm_bicycle_requests(points, count):
    requests_dm_bicycle_list = []
    for i in range(0, count):
        routing_data = {
              "points": [
                {
                  "lon": points[i]['x1'],
                  "lat": points[i]['y1'],
                  "type": "walking",
                  "idx": 0
                },
                {
                  "lon": points[i]['x2'],
                  "lat": points[i]['y2'],
                  "type": "walking",
                  "idx": 1
                }
              ],
              "sources": [
                0
              ],
              "targets": [
                1
              ],
              "mode": "bicycle",
              "detailed": True,
              "start_time": "2021-10-27T15:00:00Z",
              "exclude": [],
              "type": "statistics"
            }
        requests_dm_bicycle_list.append(routing_data)
    return requests_dm_bicycle_list


def load_pairs_bicycle_requests(points, count):
    requests_pairs_bicycle_list = []
    for i in range(0, count):
        routing_data = {
                "points": [
                    {
                        "type1": "walking",
                        "type2": "walking",
                        "lon1": points[i]['x1'],
                        "lat1": points[i]['y1'],
                        "lon2": points[i]['x2'],
                        "lat2": points[i]['y2']
                    }
                ],
                "output": "full",
                "type": "statistics",
                "utc": 1635346800
            }
        requests_pairs_bicycle_list.append(routing_data)
    return requests_pairs_bicycle_list


def run_load(url1, url2, requests_dm_bicycle_list, requests_pairs_bicycle_list):

    routes_count = len(requests_dm_bicycle_list)
    dm_bicycle_distance_list = []
    dm_bicycle_duration_list = []
    for i, dm_bicycle_request in enumerate(requests_dm_bicycle_list):
        request_dm_bicycle_body = json.dumps(dm_bicycle_request)
        dm_bicycle_resp = requests.post(url1, data=request_dm_bicycle_body)
        dm_bicycle_resp.raise_for_status()

        d_b_json_resp = dm_bicycle_resp.json()
        dm_bicycle_distance = d_b_json_resp["routes"][0]["distance"]
        dm_bicycle_duration = d_b_json_resp["routes"][0]["duration"]
        dm_bicycle_distance_list.append(dm_bicycle_distance)
        dm_bicycle_duration_list.append(dm_bicycle_duration)

    pairs_bicycle_distance_list = []
    pairs_bicycle_duration_list = []
    for i, pairs_bicycle_request in enumerate(requests_pairs_bicycle_list):
        request_pairs_bicycle_body = json.dumps(pairs_bicycle_request)
        pairs_bicycle_resp = requests.post(url2, data=request_pairs_bicycle_body)
        pairs_bicycle_resp.raise_for_status()

        p_b_json_resp = pairs_bicycle_resp.json()
        pairs_bicycle_distance = p_b_json_resp[0]["distance"]
        pairs_bicycle_duration = p_b_json_resp[0]["duration"]
        pairs_bicycle_distance_list.append(pairs_bicycle_distance)
        pairs_bicycle_duration_list.append(pairs_bicycle_duration)

    dist_diff = []
    dur_diff = []
    for i in range(routes_count):
        d_diff = abs(int(dm_bicycle_distance_list[i]) - int(pairs_bicycle_distance_list[i]))
        dist_diff.append(d_diff)
        du_diff = abs(int(dm_bicycle_duration_list[i]) - int(pairs_bicycle_duration_list[i]))
        dur_diff.append(du_diff)

    data = ['dm_bicycle_distance, pairs_bicycle_distance, dist_diff, dm_bicycle_duration,'
            'pairs_bicycle_duration, dur_diff']
    for i in range(routes_count):
        text = f'{dm_bicycle_distance_list[i]},' \
               f'{pairs_bicycle_distance_list[i]},' \
               f'{dist_diff[i]},' \
               f'{dm_bicycle_duration_list[i]} ,' \
               f'{pairs_bicycle_duration_list[i]},' \
               f'{dur_diff[i]}'

        data.append(text)

    result_file = open('nsk_bicycle_results.csv', 'w')  # файлик с результатами, с новым прогоном сохраняются новые данные
    result_file.write('\n'.join(data) + '\n')
    result_file.close()


points = load_points('nsk.csv')
requests_dm_bicycle_list = load_dm_bicycle_requests(points, 100)
with open(os.path.join("./request_bicycle_dump", 'nsk_dm_bicycle_requests.json'), "w") as file:
    json.dump(requests_dm_bicycle_list, file)
requests_pairs_bicycle_list = load_pairs_bicycle_requests(points, 100)
with open(os.path.join("./request_bicycle_dump", 'nsk_pairs_bicycle_requests.json'), "w") as file:
    json.dump(requests_pairs_bicycle_list, file)


url1 = 'http://server_name/get_dist_matrix'
url2 = 'http://server_name/get_pairs/1.0/bicycle'
run_load(url1=url1, url2=url2, requests_dm_bicycle_list=requests_dm_bicycle_list,
         requests_pairs_bicycle_list=requests_pairs_bicycle_list)
