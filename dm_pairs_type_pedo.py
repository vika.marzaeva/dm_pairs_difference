# coding=utf8
import os
import json
import requests


def load_points(file_path):
    points = []
    with open(file_path) as file:
        for line in file.readlines()[1:]:
            words = line.strip().split(",")
            points.append({"x1": float(words[0]), "y1": float(words[1]), "x2": float(words[2]), "y2": float(words[3])})
    return points


def load_dm_pedo_requests(points, count):
    requests_dm_pedo_list = []
    for i in range(0, count):
        routing_data = {
              "points": [
                {
                  "lon": points[i]['x1'],
                  "lat": points[i]['y1'],
                  "type": "walking",
                  "idx": 0
                },
                {
                  "lon": points[i]['x2'],
                  "lat": points[i]['y2'],
                  "type": "walking",
                  "idx": 1
                }
              ],
              "sources": [
                0
              ],
              "targets": [
                1
              ],
              "mode": "walking",
              "detailed": True,
              "start_time": "2021-10-27T15:00:00Z",
              "exclude": [],
              "type": "statistics"
            }
        requests_dm_pedo_list.append(routing_data)
    return requests_dm_pedo_list


def load_pair_pedo_requests(points, count):
    requests_pairs_pedo_list = []
    for i in range(0, count):
        routing_data = {
                "points": [
                    {
                        "type1": "walking",
                        "type2": "walking",
                        "lon1": points[i]['x1'],
                        "lat1": points[i]['y1'],
                        "lon2": points[i]['x2'],
                        "lat2": points[i]['y2']
                    }
                ],
                "output": "full",
                "type": "statistics",
                "utc": 1635346800
            }
        requests_pairs_pedo_list.append(routing_data)
    return requests_pairs_pedo_list


def run_load(url1, url2, requests_dm_pedo_list, requests_pairs_pedo_list):

    routes_count = len(requests_dm_pedo_list)
    dm_pedo_distance_list = []
    dm_pedo_duration_list = []
    for i, dm_pedo_request in enumerate(requests_dm_pedo_list):
        request_dm_pedo_body = json.dumps(dm_pedo_request)
        dm_pedo_resp = requests.post(url1, data=request_dm_pedo_body)
        dm_pedo_resp.raise_for_status()

        d_b_json_resp = dm_pedo_resp.json()
        dm_pedo_distance = d_b_json_resp["routes"][0]["distance"]
        dm_pedo_durations = d_b_json_resp["routes"][0]["duration"]
        dm_pedo_distance_list.append(dm_pedo_distance)
        dm_pedo_duration_list.append(dm_pedo_durations)

    pairs_pedo_distance_list = []
    pairs_pedo_duration_list = []
    for i, pairs_pedo_request in enumerate(requests_pairs_pedo_list):
        request_pairs_pedo_body = json.dumps(pairs_pedo_request)
        pairs_pedo_resp = requests.post(url2, data=request_pairs_pedo_body)
        pairs_pedo_resp.raise_for_status()

        p_b_json_resp = pairs_pedo_resp.json()
        pairs_pedo_distance = p_b_json_resp[0]["distance"]
        pairs_pedo_duration = p_b_json_resp[0]["duration"]
        pairs_pedo_distance_list.append(pairs_pedo_distance)
        pairs_pedo_duration_list.append(pairs_pedo_duration)


    dist_diff = []
    dur_diff = []
    for i in range(routes_count):
        d_diff = abs(int(dm_pedo_distance_list[i]) - int(pairs_pedo_distance_list[i]))
        dist_diff.append(d_diff)
        du_diff = abs(int(dm_pedo_duration_list[i]) - int(pairs_pedo_duration_list[i]))
        dur_diff.append(du_diff)

    data = ['dm_pedo_distance, pairs_pedo_distance, dist_diff, dm_pedo_duration,'
            'pairs_pedo_duration, dur_diff']
    for i in range(routes_count):
        text = f'{dm_pedo_distance_list[i]},' \
               f'{pairs_pedo_distance_list[i]},' \
               f'{dist_diff[i]},' \
               f'{dm_pedo_duration_list[i]} ,' \
               f'{pairs_pedo_duration_list[i]},' \
               f'{dur_diff[i]}'

        data.append(text)

    result_file = open('nsk_pedo_results.csv', 'w')  # файлик с результатами, с новым прогоном сохраняются новые данные
    result_file.write('\n'.join(data) + '\n')
    result_file.close()


points = load_points('nsk.csv')
requests_dm_pedo_list = load_dm_pedo_requests(points, 100)
with open(os.path.join("./request_pedo_dump", 'nsk_dm_pedo_requests.json'), "w") as file:
    json.dump(requests_dm_pedo_list, file)
requests_pairs_pedo_list = load_pair_pedo_requests(points, 100)
with open(os.path.join("./request_pedo_dump", 'nsk_pairs_pedo_requests.json'), "w") as file:
    json.dump(requests_pairs_pedo_list, file)


url1 = 'http://server_name/get_dist_matrix'
url2 = 'http://server_name/get_pairs/1.0/pedestrian'
run_load(url1=url1, url2=url2, requests_dm_pedo_list=requests_dm_pedo_list,
         requests_pairs_pedo_list=requests_pairs_pedo_list)
